<?php
/**
 * @file
 * Main module file for apachesolr_panels_index module.
 */

/**
 * Alter panel page forms to act on submissions.
 * @param type $form
 * @param type $form_state
 * @param type $form_id
 * @SuppressWarnings(PHPMD.UnusedFormalParameters)
 */
function apachesolr_panels_index_form_alter(&$form, &$form_state, $form_id) {
  if (arg(0) === 'admin' && arg(2) === 'pages' &&
      isset($form_state['page']->task['page type']) && $form_state['page']->task['page type'] === 'custom') {
    // We are on the page manager interface and custom page.
    if ($form_id === 'page_manager_save_page_form') {
      $form['#submit'][] = 'apachesolr_panels_index_index';

      if (isset($form['save'])) {
        $form['save']['#submit'][] = 'apachesolr_panels_index_index';
      }
    }
    elseif (isset($form['buttons']['save']['#save']) && $form['buttons']['save']['#save'] === TRUE) {
      // Reassign all form submit handlers to the submit button.
      $form['buttons']['save']['#submit'] = $form['#submit'];
      $form['buttons']['save']['#submit'][] = 'apachesolr_panels_index_index';
    }

    if ($form_id === 'page_manager_page_form_delete' || $form_id === 'page_manager_disable_form') {
      // Panel page has been deleted or disabled.
      $form['#submit'][] = 'apachesolr_panels_index_delete';
    }
  }
}

/**
 * Form submit handler for adding page to solr.
 * @param type $form
 * @param type $form_state
 * @SuppressWarnings(PHPMD.UnusedFormalParameters)
 */
function apachesolr_panels_index_index($form, &$form_state) {
  $page_info = $form_state['page']->subtask['subtask'];
  _apachesolr_panels_index_add($page_info);
}

/**
 * New panel page has been added.
 * Add the newly created panel page to the solr_nan table and the index.
 * Reset the 'last_index' time on update of the page to trigger reindex.
 * @param type $page_info
 * @SuppressWarnings(PHPMD.ElseExpression)
 */
function _apachesolr_panels_index_add($page_info) {
  // Set the items into the database.
  $paths = db_select('apachesolr_nan_index_paths', 'p')->fields('p', array('id', 'path'))->execute()->fetchAllKeyed();

  // Anonymous user is allowed to view this page, index the page.
  // Build the record array for this page.
  $record = array();
  $record['title'] = $page_info->admin_title;
  $record['path'] = $page_info->path;

  // Reindex the page daily.
  $record['frequency'] = 86400;

  // Search for existing page.
  $key = array_search($record['path'], $paths);
  if ($key) {
    // Page is already in the apachesolr_nan table.
    // Reset the last indexed value to reindex the page after update.
    $record = $record + array('id' => $key, 'last_indexed' => 0);
    drupal_write_record('apachesolr_nan_index_paths', $record, array('id'));
    drupal_set_message(t('@path will be updated in the SOLR index after the next cron run.', array('@path' => $record['path'])));
  }
  else {
    // New page, add to database.
    drupal_write_record('apachesolr_nan_index_paths', $record);
    drupal_set_message(t('@path has been added to the SOLR index and will be indexed after the next cron run.', array('@path' => $record['path'])));
  }
}

/**
 * Panel page has been deleted.
 * Remove it from the solr_nan table and index as well.
 * @param type $form
 * @param type $form_state
 * @SuppressWarnings(PHPMD.UnusedFormalParameters)
 */
function apachesolr_panels_index_delete($form, &$form_state) {
  if (isset($form_state['page']->subtask['subtask']->path)) {
    $path = $form_state['page']->subtask['subtask']->path;
    $page = db_select('apachesolr_nan_index_paths', 's_nan')
        ->fields('s_nan', array('id', 'path'))
        ->condition('path', $path, '=')
        ->execute()
        ->fetchAssoc();

    if (isset($page['id'])) {
      module_load_include('inc', 'apachesolr', 'apachesolr.index');
      $page_id = $page['id'] * -1;
      $env = variable_get('apachesolr_nan_nan_env', 'solr');
      if (apachesolr_index_delete_entity_from_index($env, 'node', $page_id)) {
        if (db_delete('apachesolr_nan_index_paths')->condition('id', $page['id'], '=')->execute()) {
          drupal_set_message(t('The page with url @url been deleted from the SOLR index.', array('@url' => $path)));
        }
      }
    }
  }
}
